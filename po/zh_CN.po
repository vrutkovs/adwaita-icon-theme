# Simplified Chinese translation for gnome-icon-theme.
# Copyright (C) 2003 Free Software Foundation, Inc.
# Abel Cheung <maddog@linux.org.hk>, 2003.
# He Qiangqiang <carton@linux.net.cn>, 2003.
# Funda Wang <fundawang@linux.net.cn>, 2003,2004.
# Tong Hui <tonghuix@gmail.com>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-icon-theme\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=adwaita-icon-theme&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2014-08-03 02:26+0000\n"
"PO-Revision-Date: 2014-08-03 18:43+0800\n"
"Last-Translator: Tong Hui <tonghuix@gmail.com>\n"
"Language-Team: zh_CN <i18n-translation@lists.linux.net.cn>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../index.theme.in.in.h:1
msgid "Adwaita"
msgstr "Adwaita"

#: ../index.theme.in.in.h:2
msgid "The Only One"
msgstr "惟一的"

#~ msgid "OK"
#~ msgstr "好"

# 48x48/emblems/emblem-art.icon.in.h:1
#~ msgid "Art"
#~ msgstr "艺术"

# 48x48/emblems/emblem-camera.icon.in.h:1
#~ msgid "Camera"
#~ msgstr "相机"

#~ msgid "Certified"
#~ msgstr "认证"

# 48x48/emblems/emblem-cool.icon.in.h:1
#~ msgid "Cool"
#~ msgstr "酷"

# 48x48/emblems/emblem-danger.icon.in.h:1
#~ msgid "Danger"
#~ msgstr "危险"

#~ msgid "Desktop"
#~ msgstr "桌面"

#~ msgid "Distinguished"
#~ msgstr "优等"

#~ msgid "Draft"
#~ msgstr "草稿"

# 48x48/emblems/emblem-handshake.icon.in.h:1
#~ msgid "Handshake"
#~ msgstr "握手"

# 48x48/emblems/emblem-marketing.icon.in.h:1
# 48x48/emblems/emblem-marketing.icon.in.h:1
#~ msgid "Marketing"
#~ msgstr "营销"

# 48x48/emblems/emblem-money.icon.in.h:1
#~ msgid "Money"
#~ msgstr "金融"

# 48x48/emblems/emblem-multimedia.icon.in.h:1
#~ msgid "Multimedia"
#~ msgstr "多媒体"

#~ msgid "Note"
#~ msgstr "备忘"

# 48x48/emblems/emblem-ohno.icon.in.h:1
#~ msgid "Oh no!"
#~ msgstr "不！"

# 48x48/emblems/emblem-people.icon.in.h:1
#~ msgid "People"
#~ msgstr "人们"

#~ msgid "Personal"
#~ msgstr "私人"

# 48x48/emblems/emblem-pictures.icon.in.h:1
#~ msgid "Pictures"
#~ msgstr "图片"

# 48x48/emblems/emblem-plan.icon.in.h:1
# 48x48/emblems/emblem-plan.icon.in.h:1
#~ msgid "Plan"
#~ msgstr "计划"

# 48x48/emblems/emblem-presentation.icon.in.h:1
#~ msgid "Presentation"
#~ msgstr "展示"

# 48x48/emblems/emblem-sales.icon.in.h:1
# 48x48/emblems/emblem-sales.icon.in.h:1
#~ msgid "Sales"
#~ msgstr "销售"

# 48x48/emblems/emblem-sound.icon.in.h:1
#~ msgid "Sound"
#~ msgstr "音效"

#~ msgid "Trash"
#~ msgstr "回收站"

#~ msgid "Default GNOME Theme"
#~ msgstr "GNOME 默认主题"

#~ msgid "GNOME"
#~ msgstr "GNOME"

# 48x48/emblems/emblem-documents.icon.in.h:1
#~ msgid "Documents"
#~ msgstr "文档"

#~ msgid "Downloads"
#~ msgstr "下载"

# 48x48/emblems/emblem-favorite.icon.in.h:1
#~ msgid "Favorite"
#~ msgstr "最爱"

#~ msgid "Generic"
#~ msgstr "一般"

#~ msgid "Important"
#~ msgstr "重要"

# 48x48/emblems/emblem-mail.icon.in.h:1
#~ msgid "Mail"
#~ msgstr "邮件"

#~ msgid "New"
#~ msgstr "新"

# 48x48/emblems/emblem-package.icon.in.h:1
#~ msgid "Package"
#~ msgstr "软件包"

#~ msgid "Photos"
#~ msgstr "照片"

# 48x48/emblems/emblem-favorite.icon.in.h:1
#~ msgid "No write"
#~ msgstr "禁写"

#~ msgid "No read"
#~ msgstr "禁读"

#~ msgid "Urgent"
#~ msgstr "紧急"

# 48x48/emblems/emblem-web.icon.in.h:1
#~ msgid "Web"
#~ msgstr "网页"
