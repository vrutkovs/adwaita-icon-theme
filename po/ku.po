# Kurdish translation for gnome-icon-theme.
# Copyright (C) 2010 gnome-icon-theme's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-icon-theme package.
# argistitori <burhanerdinc@gmail.com>, 2010.
# Erdal Ronahî <erdal dot ronahi at gmail dot com>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: gnome-icon-theme master\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-"
"icon-theme&component=general\n"
"POT-Creation-Date: 2010-10-12 21:01+0000\n"
"PO-Revision-Date: 2010-10-26 19:38+0200\n"
"Last-Translator: Erdal Ronahî <erdal dot ronahi at gmail dot com>\n"
"Language-Team: Kurdish Team http://pckurd.net\n"
"Language: ku\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Virtaal 0.6.1\n"

#: ../index.theme.in.in.h:1
msgid "Default GNOME Theme"
msgstr "Dirba GNOME ya standard"

#: ../index.theme.in.in.h:2
msgid "GNOME"
msgstr "GNOME"
