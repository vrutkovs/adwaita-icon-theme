# translation of gnome-icon-theme.gnome-2-6.he.po to Hebrew
# translation of gnome-icon-theme.HEAD.he.po to Hebrew
# translation of gnome-icon-theme.HEAD.po to Hebrew
# This file is distributed under the same license as the PACKAGE package.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER.
# Gil 'Dolfin' Osher <dolfin@rpg.org.il>, 2002,2003.
# Gil Osher <dolfin@rpg.org.il>, 2004.
# Yosef Or Boczko <yoseforb@gnome.org>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-icon-theme.HEAD.he\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-04-27 03:29+0300\n"
"PO-Revision-Date: 2014-04-27 03:30+0300\n"
"Last-Translator: Yosef Or Boczko <yoseforb@gnome.org>\n"
"Language-Team: עברית <>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Gtranslator 2.91.6\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../index.theme.in.in.h:1
msgid "Adwaita"
msgstr "Adwaita"

#: ../index.theme.in.in.h:2
msgid "The Only One"
msgstr "האחד והיחיד"

#~ msgid "OK"
#~ msgstr "אישור"

# 48x48/emblems/emblem-art.icon.in.h:1
# 48x48/emblems/emblem-art.icon.in.h:1
#~ msgid "Art"
#~ msgstr "אומנות"

# 48x48/emblems/emblem-camera.icon.in.h:1
# 48x48/emblems/emblem-camera.icon.in.h:1
#~ msgid "Camera"
#~ msgstr "מצלמה"

#~ msgid "Certified"
#~ msgstr "מאושר"

# 48x48/emblems/emblem-cool.icon.in.h:1
#~ msgid "Cool"
#~ msgstr "מגניב"

# 48x48/emblems/emblem-danger.icon.in.h:1
#~ msgid "Danger"
#~ msgstr "מסוכן"

#~ msgid "Desktop"
#~ msgstr "שולחן עבודה"

#~ msgid "Distinguished"
#~ msgstr "מכובד"

#~ msgid "Draft"
#~ msgstr "טיוטה"

# 48x48/emblems/emblem-handshake.icon.in.h:1
#~ msgid "Handshake"
#~ msgstr "לחיצת יד"

# 48x48/emblems/emblem-marketing.icon.in.h:1
#~ msgid "Marketing"
#~ msgstr "שיווק"

# 48x48/emblems/emblem-money.icon.in.h:1
#~ msgid "Money"
#~ msgstr "כסף"

# 48x48/emblems/emblem-multimedia.icon.in.h:1
# 48x48/emblems/emblem-multimedia.icon.in.h:1
#~ msgid "Multimedia"
#~ msgstr "מולטימדיה"

#~ msgid "Note"
#~ msgstr "הערה"

# 48x48/emblems/emblem-ohno.icon.in.h:1
#~ msgid "Oh no!"
#~ msgstr "הו לא!"

# 48x48/emblems/emblem-people.icon.in.h:1
#~ msgid "People"
#~ msgstr "אנשים"

#~ msgid "Personal"
#~ msgstr "אישי"

# 48x48/emblems/emblem-pictures.icon.in.h:1
# 48x48/emblems/emblem-pictures.icon.in.h:1
#~ msgid "Pictures"
#~ msgstr "תמונות"

# 48x48/emblems/emblem-plan.icon.in.h:1
#~ msgid "Plan"
#~ msgstr "תכנון"

# 48x48/emblems/emblem-presentation.icon.in.h:1
#~ msgid "Presentation"
#~ msgstr "מצגת"

# 48x48/emblems/emblem-sales.icon.in.h:1
#~ msgid "Sales"
#~ msgstr "מכירות"

# 48x48/emblems/emblem-sound.icon.in.h:1
# 48x48/emblems/emblem-sound.icon.in.h:1
#~ msgid "Sound"
#~ msgstr "קול"

#~ msgid "Trash"
#~ msgstr "אשפה"

#~ msgid "Default GNOME Theme"
#~ msgstr "ערכת הנושא ברירת המחדל של Gnome"

#~ msgid "GNOME"
#~ msgstr "GNOME"

# 48x48/emblems/emblem-documents.icon.in.h:1
# 48x48/emblems/emblem-documents.icon.in.h:1
#~ msgid "Documents"
#~ msgstr "מסמכים"

#~ msgid "Downloads"
#~ msgstr "הורדות"

# 48x48/emblems/emblem-favorite.icon.in.h:1
#~ msgid "Favorite"
#~ msgstr "מועדף"

#~ msgid "Generic"
#~ msgstr "כללי"

#~ msgid "Important"
#~ msgstr "חשוב"

# 48x48/emblems/emblem-mail.icon.in.h:1
# 48x48/emblems/emblem-mail.icon.in.h:1
#~ msgid "Mail"
#~ msgstr "דואר"

#~ msgid "New"
#~ msgstr "חדש"

# 48x48/emblems/emblem-package.icon.in.h:1
# 48x48/emblems/emblem-package.icon.in.h:1
#~ msgid "Package"
#~ msgstr "חבילה"

#~ msgid "Photos"
#~ msgstr "תמונות"

# 48x48/emblems/emblem-favorite.icon.in.h:1
#~ msgid "No write"
#~ msgstr "לא נכתב"

#~ msgid "No read"
#~ msgstr "לא נקרא"

#~ msgid "Urgent"
#~ msgstr "דחוף"

# 48x48/emblems/emblem-web.icon.in.h:1
# 48x48/emblems/emblem-web.icon.in.h:1
#~ msgid "Web"
#~ msgstr "רשת"

# 48x48/emblems/emblem-bin.icon.in.h:1
#~ msgid "Binary"
#~ msgstr "בינארי"

#~ msgid "cvs-added"
#~ msgstr "נוסף ל cvs"

#~ msgid "cvs-conflict"
#~ msgstr "התנגשות ב cvs"

#~ msgid "cvs-controlled"
#~ msgstr "נשלט על-ידי cvs"

#~ msgid "cvs-modified"
#~ msgstr "נערך ב cvs"

#~ msgid "cvs-new-file"
#~ msgstr "קובץ חדש ב cvs"

#~ msgid "cvs-removed"
#~ msgstr "הוסר ב cvs"

#~ msgid "cvs-sticky-tag"
#~ msgstr "תג דביק ב cvs"

#~ msgid "Symbolic link"
#~ msgstr "קישור סימבולי"

#~ msgid "Special"
#~ msgstr "מיוחד"
