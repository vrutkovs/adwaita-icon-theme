# translation of pa.po to Punjabi
# translation of gnome-icon-theme.HEAD.pa.po to Punjabi
# This file is distributed under the same license as the PACKAGE package.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER.
# Amanpreet Singh Alam <aalam@redhat.com>, 2004.
# Amanpreet Singh Alam <amanpreetalam@yahoo.com>, 2005.
# A S Alam <aalam@users.sf.net>, 2009, 2014.
msgid ""
msgstr ""
"Project-Id-Version: pa\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=adwaita-icon-theme&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2014-04-25 16:29+0000\n"
"PO-Revision-Date: 2014-09-21 08:46-0500\n"
"Last-Translator: A S Alam <aalam@users.sf.net>\n"
"Language-Team: Punjabi/Panjabi <punjab-l10n@list.sf.net>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"\n"
"\n"

#: ../index.theme.in.in.h:1
msgid "Adwaita"
msgstr "ਵਿਲੱਖਣ"

#: ../index.theme.in.in.h:2
msgid "The Only One"
msgstr "ਕੇਵਲ ਇੱਕ"

#~ msgid "Default GNOME Theme"
#~ msgstr "ਡਿਫਾਲਟ ਗਨੋਮ ਥੀਮ"

#~ msgid "GNOME"
#~ msgstr "ਗਨੋਮ"

#~ msgid "Desktop"
#~ msgstr "ਡੈਸਕਟਾਪ"

#~ msgid "Documents"
#~ msgstr "ਡੌਕੂਮੈਂਟ"

#~ msgid "Downloads"
#~ msgstr "ਡਾਊਨਲੋਡ"

#~ msgid "Favorite"
#~ msgstr "ਮਨਪਸੰਦ"

#~ msgid "Generic"
#~ msgstr "ਆਮ"

#~ msgid "Important"
#~ msgstr "ਖਾਸ"

#~ msgid "Mail"
#~ msgstr "ਮੇਲ"

#~ msgid "New"
#~ msgstr "ਨਵਾਂ"

#~ msgid "Package"
#~ msgstr "ਪੈਕੇਜ"

#~ msgid "Photos"
#~ msgstr "ਫੋਟੋ"

#~ msgid "No write"
#~ msgstr "ਲਿਖਿਆ ਨਹੀਂ"

#~ msgid "No read"
#~ msgstr "ਪੜ੍ਹਿਆ ਨਹੀਂ"

#~ msgid "Urgent"
#~ msgstr "ਲਾਜ਼ਮੀ"

#~ msgid "Web"
#~ msgstr "ਵੈੱਬ"

#~ msgid "OK"
#~ msgstr "ਠੀਕ ਹੈ"

#~ msgid "Art"
#~ msgstr "ਕਲਾ"

#~ msgid "Binary"
#~ msgstr "ਬਾਈਨਰੀ"

#~ msgid "Camera"
#~ msgstr "ਕੈਮਰਾ"

#~ msgid "Certified"
#~ msgstr "ਪ੍ਰਮਾਣਿਤ"

#~ msgid "Cool"
#~ msgstr "ਠੰਡਾ"

#~ msgid "cvs-added"
#~ msgstr "cvs-added"

#~ msgid "cvs-conflict"
#~ msgstr "cvs-conflict"

#~ msgid "cvs-controlled"
#~ msgstr "cvs-controlled"

#~ msgid "cvs-modified"
#~ msgstr "cvs-modified"

#~ msgid "cvs-new-file"
#~ msgstr "cvs-new-file"

#~ msgid "cvs-removed"
#~ msgstr "cvs-removed"

#~ msgid "cvs-sticky-tag"
#~ msgstr "cvs-sticky-tag"

#~ msgid "Danger"
#~ msgstr "ਖਤਰਾ"

#~ msgid "Distinguished"
#~ msgstr "ਵੱਖਰਾ"

#~ msgid "Draft"
#~ msgstr "ਡਰਾਫਟ"

#~ msgid "Handshake"
#~ msgstr "ਹੈਡਸ਼ੇਕ"

#~ msgid "Marketing"
#~ msgstr "ਵਪਾਰਕ"

#~ msgid "Money"
#~ msgstr "ਪੈਸਾ"

#~ msgid "Multimedia"
#~ msgstr "ਬਹੁਰੰਗ"

#~ msgid "Note"
#~ msgstr "ਸੂਚਨਾ"

#~ msgid "Oh no!"
#~ msgstr "ਓਹ ਹੋ!"

#~ msgid "People"
#~ msgstr "ਲੋਕ"

#~ msgid "Personal"
#~ msgstr "ਨਿੱਜੀ"

#~ msgid "Pictures"
#~ msgstr "ਤਸਵੀਰ"

#~ msgid "Plan"
#~ msgstr "ਨੀਤੀ"

#~ msgid "Presentation"
#~ msgstr "ਪੇਸ਼ਕਾਰੀ"

#~ msgid "Sales"
#~ msgstr "ਵਿਕਾਊ"

#~ msgid "Shared"
#~ msgstr "ਸਾਂਝ"

#~ msgid "Sound"
#~ msgstr "ਅਵਾਜ਼"

#~ msgid "Symbolic link"
#~ msgstr "ਨਿਸ਼ਾਨ ਸੰਬੰਧ"

#~ msgid "Special"
#~ msgstr "ਖਾਸ"

#~ msgid "Trash"
#~ msgstr "ਰੱਦੀ"
